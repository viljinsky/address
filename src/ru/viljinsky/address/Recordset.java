/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.viljinsky.address;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Класс для основной работы с набором данных
 *
 * @author viljinsky
 */
public class Recordset extends ArrayList<Object[]> {

    static class IntegerComparator implements Comparator<Object[]> {

        int index;

        public IntegerComparator(int index) {
            this.index = index;
        }

        @Override
        public int compare(Object[] o1, Object[] o2) {
            return ((Integer) o1[index]).compareTo((Integer) o2[index]);
        }
    }

    static class StringComparator implements Comparator<Object[]> {

        int index;

        public StringComparator(int index) {
            this.index = index;
        }

        @Override
        public int compare(Object[] o1, Object[] o2) {
            return String.valueOf(o1[index]).compareTo(String.valueOf(o2[index]));
        }
    }

    class ValuesIterator implements Iterator<Values> {

        Integer index = 0;

        @Override
        public boolean hasNext() {
            return index < size();
        }

        @Override
        public Values next() {
            return getValues(index++);
        }
    }
    static final String COUNT = "count";
    List<String> columns;

    public Recordset(String... columns) {
        this.columns = Arrays.asList(columns);
    }

    public Recordset(Recordset recordset) {
        this.columns = new ArrayList<>(recordset.columns);
    }

    int columnIndex(String columnName) {
        return columns.indexOf(columnName);
    }

    @Override
    public boolean add(Object[] e) {
        if (e.length != columns.size()) {
            throw new IllegalArgumentException("Bad values" + Arrays.toString(e));
        }
        return super.add(e);
    }

    public void add(Values values) {
        Object[] p = new Object[columns.size()];
        for (int i = 0; i < p.length; i++) {
            if (values.containsKey(columns.get(i))) {
                p[i] = values.get(columns.get(i));
            }
        }
        add(p);
    }

    Iterable<Values> values() {
        return () -> new ValuesIterator();
    }

    String tab(Object p) {
        int length = 16;
        String s = String.valueOf(p);
        char[] str = Arrays.copyOf(s.toCharArray(), length);
        if (s.length() < length) {
            Arrays.fill(str, s.length(), length, ' ');
        }
        return new String(str);
    }

    public void print() {
        print("Recordset");
    }

    public void print(String label) {
        if (label != null) {
            System.out.println(label);
        }
        columns.forEach(column -> {
            System.out.print(tab(column));
        });
        System.out.println();
        forEach(p -> {
            for (Object p1 : p) {
                System.out.print(tab(p1) + " ");
            }
            System.out.println();
        });
        System.out.println("Total records : " + size() + "\n");
    }

    Values getValues(int row) {
        Object[] p = get(row);
        Values values = new Values();
        for (int i = 0; i < columns.size(); i++) {
            values.put(columns.get(i), p[i]);
        }
        return values;
    }

    /**
     * Получение первой записи по соответсвию поля (в поле с указнным номером указанное значение)
     */
    Values find(int colunnIndex, Object value) {
        int i = locate(colunnIndex, value);
        if (i < 0) {
            return null;
        }
        return getValues(i);
    }

    /**
     *  Нахождение номера записи с соответствующими значениями (все поля).
     */
    Integer locate(Object[] src) {
        label:
        for (Object[] p : this) {
            for (Integer i = 0; i < src.length; i++) {
                if (!src[i].equals(p[i])) {
                    continue label;
                }
            }
            return indexOf(p);
        }
        return -1;
    }

    /**
     * Нахождение номера записи по занчению(values) в поле с указанным номером(columnIndex).
     */
    Integer locate(int columnIndex, Object value) {
        for (Object[] p : this) {
            if (p[columnIndex] != null && p[columnIndex].equals(value)) {
                return indexOf(p);
            }
        }
        return -1;
    }

    /**
     * Срез даннх по условию VALUES
     */
    Recordset filter(Values values) {
        Map<Integer, Object> map = new HashMap<>();
        values.keySet().forEach(key -> {
            map.put(columnIndex(key), values.get(key));
        });
        Recordset recordset = new Recordset(this);
        label:
        for (Object[] p : this) {
            for (int i : map.keySet()) {
                if (!p[i].equals(map.get(i))) {
                    continue label;
                }
            }
            recordset.add(p);
        }
        return recordset;
    }

    /**
     * срез данных по указанным полям
     */
    Recordset select(String... fieldName) {
        Recordset recordset = new Recordset(fieldName);
        int columnCount = recordset.columns.size();
        Map<Integer, Integer> map = new HashMap<>();
        for (int key = 0; key < fieldName.length; key++) {
            map.put(key, columnIndex(fieldName[key]));
        }
        forEach(p -> {
            Object[] p1 = new Object[columnCount];
            map.keySet().forEach(key -> {
                p1[key] = p[map.get(key)];
            });
            recordset.add(p1);
        });
        return recordset;
    }

    /**
     * Подучение уникальных записей по всем полям
     */
    Recordset unique() {
        Recordset recordset = new Recordset(this);
        forEach(p->{
            if (recordset.locate(p) < 0) {
                recordset.add(p);
            }
        });
        return recordset;
    }

    /**
     * Подсчёт уникалных записей (кол.в выводится в поле COUNT)
     */
    Recordset count() {
        Recordset recordset = new Recordset(this);
        recordset.columns.add(COUNT);
        try {
            forEach(p->{
                int n = recordset.locate(p);
                if (n < 0) {
                    Object[] p1 = Arrays.copyOf(p, p.length + 1);
                    p1[p1.length - 1] = 1;
                    recordset.add(p1);
                } else {
                    Object[] p1 = recordset.get(n);
                    p1[p1.length - 1] = (Integer) p1[p1.length - 1] + 1;
                    recordset.set(n, p1);
                }
            });
        } catch (Exception e) {
            System.err.println("Recordset.count.exeptiin : "+e.getMessage());
        }
        return recordset;
    }

    /**
     * Подсчёт кол.ва записей по указанному полю (кол.во выводится поле Count)
     */
    Recordset count(String key) {
        int index = columnIndex(key);
        Recordset recordset = new Recordset(key, COUNT);
        forEach(p -> {
            Integer n = recordset.locate(0, p[index]);
            if (n < 0) {
                recordset.add(new Object[]{p[index], 1});
            } else {
                Object[] p1 = recordset.get(n);
                p1[1] = (Integer) p1[1] + 1;
                recordset.set(n, p1);
            }
        });
        return recordset;
    }

    Recordset sortBy(Comparator comparator) {
        sort(comparator);
        return this;
    }

    Recordset sortBy(String key) {
        sort(new StringComparator(columnIndex(key)));
        return this;
    }
    
}
