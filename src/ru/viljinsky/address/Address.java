/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.viljinsky.address;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Поля данных базы address
 *
 * @author viljinsky
 */
interface AddressColumns {
    
    static final String SITY = "city";
    static final String STREET = "street";
    static final String HOUSE = "house";
    static final String FLOOR = "floor";
    
    static final String F1 = "f1";
    static final String F2 = "f2";
    static final String F3 = "f3";
    static final String F4 = "f4";
    static final String F5 = "f5";
    
}
/**
 * Вспомогательный класс для индикации хода работы. 
 * @author viljinsky
 */
class Progress {

    long time;
    long t;
    int count;
    final char[] chars = "-\\|/-\\|//".toCharArray();
    
    public Progress() {
        this(null);
    }
    
    public Progress(String prompt) {
        count = 0;
        time = t = System.currentTimeMillis();
        
        if (prompt != null) {
            char[] c = Arrays.copyOf(prompt.toCharArray(), 25);
            Arrays.fill(c, Math.min(c.length, prompt.length()), c.length - 1, '.');
            System.out.print(new String(c));
        }
    }
    
    public void next() {
        next(null);
    }
    
    public void next(Integer delay) {
        if (System.currentTimeMillis() > t + 500) {
            count++;
            System.out.print("\b" + chars[count % 8]);
            t = System.currentTimeMillis();
        }
        delay(delay);
    }
    
    void delay(Integer ms) {
        if (ms == null) {
            return;
        }
        t = System.currentTimeMillis();
        while (System.currentTimeMillis() < t + ms) {
            // nop
        }
    }
    
    public void stop() {
        System.out.println(String.format("\bГотово (%.3f с.)", (System.currentTimeMillis() - time) / 1000.0));        
    }
}

class Checker extends Thread {
    
    interface CheckerListener {

        public void ready(Checker checker);

        public void progress();
    }
    
    CheckerListener checkerListener;
    
    Recordset recordset;
    Recordset tmp;
    
    public Checker(CheckerListener checkerListener, Recordset recordset) {
        this.checkerListener = checkerListener;
        this.recordset = recordset;
        this.tmp = new Recordset(recordset);
    }
    
    @Override
    public void run() {
        Recordset t = recordset.select(AddressColumns.STREET).unique();
        for (Values v : t.values()) {
            List list = new ArrayList();
            Recordset t2 = recordset.filter(v.getValues(AddressColumns.STREET));
            t2.forEach(p -> {
                int n = (int) p[2] * 100 + (int) p[3];
                if (list.contains(n)) {
                    tmp.add(p);
                } else {
                    list.add(n);
                }
                checkerListener.progress();
            });
        }
        checkerListener.ready(this);
    }
    
}

/**
 * Вспомогательный класс для улучшения скорости проверки уникальных запией
 * @author viljinsky
 */
class CheckerList implements Checker.CheckerListener, AddressColumns {

    // address
    Recordset recordset;
    Recordset tmp;
    int count = 0;
    Progress progress;
    
    @Override
    public void ready(Checker checker) {
        synchronized (this) {
            tmp.addAll(checker.tmp);
            count--;
        }
    }
    
    @Override
    public void progress() {
        synchronized (this) {
            progress.next();
        }
    }
    
    public CheckerList(Recordset recordset) {
        this.recordset = recordset;
        this.tmp = new Recordset(recordset);
        
    }
    
    public void addChecker(Recordset recordset) {
        count++;
        Checker checker = new Checker(this, recordset);
        checker.start();
    }

    // recordset - sity
    public void execute(Recordset sity) {
        progress = new Progress("Проверка");
        for (Values values : sity.values()) {
            Recordset t = recordset.filter(values.getValues(SITY));
            addChecker(t);
        }
        while (count > 0) {
            progress.next(100);
            
        }
        progress.stop();
    }
    
}

/**
 * База данных address
 *
 * @author viljinsky
 */
public class Address extends Recordset implements AddressColumns {
    
    public static Progress progress;
    
    public Address() {
        super(SITY, STREET, HOUSE, FLOOR);
    }
    
    public Address(String fileName) throws Exception {
        this(fileName, -1);
    }
    
    public Address(String fileName, Integer maxSize) throws Exception {
        this();
        AddressParser parser;
        if (fileName.endsWith(".xml")) {
            parser = new AddressXML(this, fileName,maxSize);
        } else if (fileName.endsWith(".xls")) {
            parser = new AddressCSV(this, fileName,maxSize);
        } else {
            throw new IllegalArgumentException("Файл должен иметь расширение xml или xls");
        }
        
        System.out.println(fileName);
        System.out.println("------------------");
        parser.read();
        addAll(parser);
    }
    
    public Address(Recordset recordset) {
        this();
        if (!recordset.columns.equals(this.columns)) {
            throw new IllegalArgumentException("source mast have same field as Address");
        }
        addAll(recordset);
    }

    /**
     * Список городов
     *
     * @return
     */
    public Recordset sity() {
        return count(SITY);
    }

    /**
     * Список улиц города
     *
     * @param sity
     * @return
     */
    public Recordset street(Object sity) {
        return filter(new Values(SITY, sity)).select(SITY, STREET).unique().sortBy(STREET);
    }

    /**
     * Список этажности в городе
     *
     * @param sity
     * @return
     */
    public Recordset floor(Object sity) {
        return select(SITY, FLOOR).filter(new Values(SITY, sity)).count(FLOOR);
    }

    /**
     * Список домов города
     *
     * @param sity
     * @return
     */
    public Recordset house(Object sity) {
        return filter(new Values(SITY, sity)).select(STREET, HOUSE).unique();
    }

    /*
    * Карта кол.этажей->поле отчёта
    */
    static final Map<Object,String> mapFloor = new HashMap<>();
    static{
        mapFloor.put(1, F1);
        mapFloor.put(2, F2);
        mapFloor.put(3, F3);
        mapFloor.put(4, F4);
        mapFloor.put(5, F5);
    }
    /**
     * Подсчёт кол.-ва 1 2 3 4 5 этажных домов по городам.
     *
     * @return
     */
    public Recordset argegate() {
        progress = new Progress("Составление отчёта");
        Recordset tmp = new Recordset(SITY, F1, F2, F3, F4, F5);
        
        for (Values values : sity().values()) {
            Values p = new Values();
            p.put(SITY, values.get(SITY));
            for (Values v2 : floor(values.get(SITY)).values()) {
                if (mapFloor.containsKey(v2.get(FLOOR))){
                    p.put(mapFloor.get(v2.get(FLOOR)), v2.get(Recordset.COUNT));
                }
                progress.next();
            }
            tmp.add(p);
        }
        progress.stop();
        return tmp.sortBy(new Recordset.StringComparator(tmp.columnIndex(SITY)));
    }
    
    public Recordset check() {
        CheckerList checkerList = new CheckerList(this);
        checkerList.execute(sity());
        Recordset tmp = new Recordset(this);
        for (Values v : checkerList.tmp.unique().values()) {
            tmp.addAll(filter(v));
        }
        return tmp.count();
    }
    
    public void execute(String fileName) {
        
        try {
            // чтение всех данных
            Address address = new Address(fileName, -10000);

            // Проверка повторяющихся записей
            address.check().print("Повторяющиеся записи");

            // ответ на главны вопрос задачи.
            address.argegate().print("Кол. 1, 2, 3, 4, 5 - этажных домов по городам");
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        
    }
    
}
