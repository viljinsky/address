/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.viljinsky.address;

import java.util.HashMap;

/**
 * Вспомогатльный классс для работы с набором данных
 *
 * @author viljinsky
 */
public class Values extends HashMap<String, Object> {

    public Values() {
    }

    public Values(String key, Object value) {
        put(key, value);
    }

    public Values getValues(String... keys) {
        Values values = new Values();
        for (String key : keys) {
            if (!containsKey(key)) {
                throw new IllegalArgumentException("field " + key + " not found");
            }
            values.put(key, get(key));
        }
        return values;
    }

    public void print() {
        keySet().forEach(columnName -> {
            System.out.print(columnName + " : " + get(columnName) + ", ");
        });
        System.out.println();
    }
    
}
