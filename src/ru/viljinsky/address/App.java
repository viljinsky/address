/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.viljinsky.address;

import java.util.Scanner;

/**
 *
 * @author viljinsky
 */
public class App implements AddressColumns {
    
    static final String promt = 
        "Введите \n"+
        "    <filename> - имя файла\n"+
        "    1          - \"address.xml\"\n"+
        "    2          - \"address.csv.xls\"\n"+
        "    .          - выход\n"+
        "ввод : ";
            
    
    public static void main(String[] args){

        String line,filename;
        
        Scanner scanner = new Scanner(System.in); 
        scanner.reset();
        while(true){
            System.out.print(promt);
            line = scanner.next();
            switch(line){
                case "." : System.exit(0);
                case "1" : filename = "address.xml";break;
                case "2" : filename = "address.csv.xls";break;
                default:
                    filename = line;
            }
            new Address().execute(filename);                
        }
        
    }

}
